package com.piserve.geejo.sunshine;

import android.test.suitebuilder.TestSuiteBuilder;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * Created by Droid Space on 2/25/2015.
 */
public class FullTestSuite extends TestSuite
{
    public static Test suite()
    {
        return new TestSuiteBuilder(FullTestSuite.class)
                .includeAllPackagesUnderHere().build();
    }

    public FullTestSuite() {
        super();
    }
}

