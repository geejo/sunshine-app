package com.piserve.geejo.sunshine;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


import java.util.ArrayList;

public class SunshineFragment extends Fragment
{
    private final String LOG_TAG = SunshineFragment.class.getSimpleName();

    /*For updating the adapter with the weather data we got*/
    private ArrayAdapter<String> mForecastAdapter;

    public SunshineFragment()
    {
        // Required empty public constructor
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        //Add this line in-order for this fragment to handle menu events
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        //Inflate the menu; this adds items to the action bar if present
        inflater.inflate(R.menu.menu_sunshine_fragment, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id==R.id.action_refresh)
        {

            //Moving functions to a helper class, so that onActivity start the data can be displayed
            updateWeather();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_sunshine, container, false);

        //Once the root view for the fragment has been created.
            /*Create an ArrayList of strings for each forecast entry. This is Fake data*/

        /* FAKE DATA NO LONGER REQUIRED AFTER REFRESH IS IMPLEMENTED DURING onSTART() AND WE
           PASS AN EMPTY ARRAY LIST INTO THE ARRAY ADAPTER


        String [] forecastArray =
                {
                        "Today - Sunny - 35/30",
                        "Tomorrow - Foggy - 33/28",
                        "Wednesday - Cloudy - 33/26",
                        "Thursday - Sleepy - 30/24",
                        "Friday - Bunking - 36/34",
                        "Saturday - Trapped - 38/35",
                        "Sunday - Heavy Rain - 32/28"
                };
        List<String> myList = Arrays.asList(forecastArray);

        List<String> weekForecast = new ArrayList<>(myList);

        */


            /*Now we have some dummy forecast data, create an ArrayAdapter. The ArrayAdapter will
            take data from a source (dummy forecast data) use it to populate the ListView it's attached to.
            ArrayAdapter acts as reference to raw data*/

        //INITIALIZE THE ADAPTER.

        // Create the ArrayAdapter<String>.
            /*Parameters required
            * 1. Context: (Global) helps to access system services, resources, appn specific resources.
            *    We use Fragment's containing Activity as Context: getActivity()
            * 2. ID of ListItem layout: R.layout.list_item_forecast
            * 3. ID of TextView: R.id.list_item_forecast_textview
            * 4. List of data: weekForecast*/

        mForecastAdapter = new ArrayAdapter<String>
                (       getActivity(), //Current context {fragment's parent activity}
                        R.layout.list_item_forecast, //ID of ListItem layout
                        R.id.list_item_forecast_textview, //ID of TextView we want to populate
                        //weekForecast //Forecast data : No longer required as we are passing an empty ArrayList now.
                        new ArrayList<String>()
                );

        //BIND THE ADAPTER TO THE LISTVIEW.

        // But there is no reference to ListView here, only in XML.
            /*1. Take and inflate XML layout file
            * 2. Turn it into a full View hierarchy (Tree form)
            * 3. Call the reference to the items in the XML using findViewById*/

            /*a) Find reference to the ListView
            * b) Set ArrayAdapter on ListView: it supplies list_item layout to ListView based on weekForecast data*/
        ListView listView = (ListView) fragmentView.findViewById(R.id.listview_forecast);
        listView.setAdapter(mForecastAdapter);

        //Set the click listener for the items in the ListView
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                //Gets the forecast data  of the item clicked
                String forecast = mForecastAdapter.getItem(position);

                /*Display the toast showing the string. It's removed later, to switch to Intent
                Toast.makeText(getActivity(), forecast, Toast.LENGTH_SHORT).show();*/

                //Create an intent pointing to Detail fragment
                Intent intent = new Intent(getActivity(),SunshineDetail.class);

                //Pass the  forecast string to the second activity
                intent.putExtra(Intent.EXTRA_TEXT,forecast);

                //start the Activity
                startActivity(intent);
            }
        });

        return fragmentView;

        //The return aspect of the code is into the developing
    }


    //Function that reads the location preference and displays weather for the same
    private void updateWeather()
    {
        /*Since we have added database friendly FetchWeatherTask we're removing FetchWeather task
        implementation in the Fragment and transferring it to data. */
//        FetchWeatherTask weatherTask = new FetchWeatherTask();

        FetchWeatherTask weatherTask = new FetchWeatherTask(getActivity(),mForecastAdapter);

        //Read from SharedPreferences
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        //Since the prefs are stored in key-value pairs, we get the value stored for location key.
        //Value stored in location key is got if specified, else it falls to default location key.
        String location = prefs.getString(getString(R.string.pref_location_key), getString(R.string.pref_location_default));

            /* Now we allow the user to give the location, so we remove the static value.
            weatherTask.execute("682017");
            */

        //Pass the location into the FetchWeatherTask
        weatherTask.execute(location);
    }

    //This makes the Refresh start whenever the fragment starts
    @Override
    public void onStart()
    {
        super.onStart();
        updateWeather();
    }


//Ever since we started using databases we no longer need this. We are implementing a different FetchWeatherTask class in data

/*


    public class FetchWeatherTask extends AsyncTask<String, Void, String[]> // <Params, Progress, Result>
    {
        private final String LOG_TAG = FetchWeatherTask.class.getSimpleName();

        @Override
        protected String[] doInBackground(String... params)
        {

            if(params.length==0)
            {
                return null;
            }

            HttpURLConnection urlConnection = null;
            //Get an HTTP client that supports both streaming uploads and downloads

            //Reads the string into the Buffer
            BufferedReader reader = null;

            //Will contain raw JSON response as String
            String forecastJsonStr = null;

            String format = "json";
            String units = "metric";
            int numDays = 7;

            try
            {
                //Construct the URL for OpenWeatherMap query
                // Possible parameters are available at OWM's forecast API page, at
                // http://openweathermap.org/API#forecast

                final String FORECAST_BASE_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?";
                final String QUERY_PARAM = "q";
                final String FORMAT_PARAM = "mode";
                final String UNITS_PARAM = "units";
                final String DAYS_PARAM = "cnt";

                Uri builtUri = Uri.parse(FORECAST_BASE_URL).buildUpon().
                        appendQueryParameter(QUERY_PARAM, params[0]).
                        appendQueryParameter(FORMAT_PARAM, format).
                        appendQueryParameter(UNITS_PARAM, units).
                        appendQueryParameter(DAYS_PARAM, Integer.toString(numDays)).
                        build();

                URL url = new URL(builtUri.toString());

                Log.v(LOG_TAG, "Built URI" +builtUri.toString());



                //Instantiate the url object with the target URL of the resource to request

                URL url = new URL("http://api.openweathermap.org/data/2.5/forecast/daily?");

                //Create the request to OpenWeatherMap and open the connection


                */
/*Instantiate the HttpUrlConnection with the url object - A new connection is opened
                 * every time by calling the openConnection method of the protocol handler by the URL. *//*


                urlConnection = (HttpURLConnection) url.openConnection();


                */
/*Set the type of request method to GET*//*


                urlConnection.setRequestMethod("GET");


                */
/*Request the server to establish a connection*//*


                urlConnection.connect();

                //READ THE INPUT STREAM TO A STRING

                */
/*Response body is read from the stream using getInputStream*//*


                InputStream inputStream = urlConnection.getInputStream();

                StringBuffer buffer = new StringBuffer();

                if(inputStream==null)
                {
                    //Nothing to be done
                    forecastJsonStr = null;
                }

                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;

                while((line= reader.readLine())!=null)
                {
                    buffer.append(line + "\n");
                }
                if(buffer.length()==0)
                {
                    //Stream was empty. No point in parsing.
                    forecastJsonStr = null;
                }

                forecastJsonStr = buffer.toString();

                //Check if data returned is correct
                Log.v(LOG_TAG, "Forecast JSON String: " + forecastJsonStr);
            }

            catch(IOException e)
            {
                Log.e("SunshineFragment", "Error ", e);
                // If the code didn't successfully get the weather data, there's no point in attempting
                // to parse it.
                forecastJsonStr = null;
            }

            finally
            {
                if (urlConnection != null)
                {
                    urlConnection.disconnect();
                }

                if (reader != null) {
                    try
                    {
                        reader.close();
                    }

                    catch (final IOException e)
                    {
                        Log.e(LOG_TAG, "Error closing stream", e);
                    }
                }
            }

            try
            {
                return getWeatherDataFromJson(forecastJsonStr, numDays);
            }

            catch(JSONException e)
            {
                Log.e(LOG_TAG, e.getMessage(),e);
                e.printStackTrace();
            }

            //This will only happen if we get an error getting  or parsing the forecast
            return null;
        }

        @Override
        protected void onPostExecute(String[] result)
        {
            mForecastAdapter.clear();

            if(result!=null)
            {

                for(String  dayForecastStr: result)
                {
                    mForecastAdapter.add(dayForecastStr);
                }

                //New data is back from the server and the set of data is
            }
        }
    }


*/


/*EXAMPLE TO EXTRACT THE VALUE OF ONE OF THE ATTRIBUTES.
      CODE TO GET THE MAX TEMPERATURE. PARSE THE REST MANUALLY*//*


    public static double getMaxTemperatureForDay(String weatherJsonStr, int dayIndex)
            throws JSONException
    {
        JSONObject weather = new JSONObject(weatherJsonStr); //Convert JSONString into JSON object
        JSONArray days = weather.getJSONArray("list");
        JSONObject dayInfo = days.getJSONObject(dayIndex);
        JSONObject temperatureInfo = dayInfo.getJSONObject("temp");

        return temperatureInfo.getDouble("max");
    }

    */
/*3 HELPER METHODS INCLUDED FOR JSON PARSING*//*


    */
/*1. Formatting dates*//*


    private String getReadableDateString(long time)
    {
        //Convert date from server [timestamp] into milliseconds to convert it into a valid date
        Date date = new Date(time*1000);

        SimpleDateFormat format = new SimpleDateFormat("E, MMM d");
        return format.format(date).toString();
    }

    */
/*2. Rounding the temperature. *//*


    private String formatHighLows(double high, double low)
    {
        */
/*Default value fetched is in Celsius. If the user wants to see in Fahrenheit, convert the values
        here. This is done rather than fetching in Fahrenheit so that the user can change this option
        without us having to re-fetch the data once we start storing the values in the database. *//*


        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        //Get the key value pair of preference stored in the Settings as string
        String unitType = sharedPrefs.getString(getString(R.string.pref_units_key), getString(R.string.pref_units_metric));

        if (unitType.equals(getString(R.string.pref_units_imperial)))
        {
            high = (high*1.8)+32;
            low = (low*1.8)+32;
        }

        else if(!unitType.equals(getString(R.string.pref_units_metric)))
        {
            Log.d(LOG_TAG,"Unit type not found: " +unitType);
        }

        long roundedHigh = Math.round(high);
        long roundedLow = Math.round(low);

        String highLowStr = roundedHigh + "/" + roundedLow;
        return highLowStr;
    }
*/

    /*3. Turn the forecast JSONString into an array of weather forecasts*/
    /*Take the String representing the complete forecast in JSON format. Next pull out the data
    we need to construct the strings needed for the wireFrames*/

}
