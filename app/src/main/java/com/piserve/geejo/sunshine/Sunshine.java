
package com.piserve.geejo.sunshine;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;


public class Sunshine extends ActionBarActivity
{

    private final String LOG_TAG = Sunshine.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sunshine);
        if (savedInstanceState == null)
        {
            //Transaction = Add or Remove a fragment
            //To perform a transaction FragmentManager is used.
            //Fragment transaction provides API's to add/remove/replace other fragment transations.
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new SunshineFragment())
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sunshine, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            //Starts the Settings activity
            startActivity(new Intent(this, SunshineSettings.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Menu is inflated here. Handle Map menu item being inflated using helper method
    public void openPreferredLocationMap()
    {
        /*Read the location from user's SharedPreferences*/
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        //Read the key-value pair from the SharedPreferences
        String location = sharedPrefs.getString
                          (
                            getString(R.string.pref_location_key),
                            getString(R.string.pref_location_default)
                          );

        //Create a View intent indicating it's location in the data URI

        Uri geoLocation = Uri.parse("geo:0,0?").buildUpon().
                appendQueryParameter("q", location).
                build();

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(geoLocation);

        //Start an activity with this Intent. The Activity is started if only it resolves successfully
        if(intent.resolveActivity(getPackageManager())!=null)
        {
            startActivity(intent);
        }

        else
        {
            Log.d(LOG_TAG, "Couldn't call" +location + "no");
        }

    }
}