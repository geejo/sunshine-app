package com.piserve.geejo.sunshine;

import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p/>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SunshineSettings extends PreferenceActivity  implements Preference.OnPreferenceChangeListener
{
    /**
     * Determines whether to always show the simplified settings UI, where
     * settings are presented in a single list. When false, settings are shown
     * as a master/detail two-pane view on tablets. When true, a single pane is
     * shown on tablets.
     */
    private static final boolean ALWAYS_SIMPLE_PREFS = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // setupActionBar();
        addPreferencesFromResource(R.xml.pref_general);

        /*When the user selects a new option in the dialog, we set the PreferenceSummary to the
        * new value of the values provided in the EditText or ListPreference set by user*/

        bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_location_key)));

        bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_units_key)));

    }

    private void bindPreferenceSummaryToValue(Preference preference)
    {
        preference.setOnPreferenceChangeListener(this);

        onPreferenceChange(preference, PreferenceManager.getDefaultSharedPreferences(preference.getContext())
                                                        .getString(preference.getKey(), ""));
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue)
    {
        String stringValue = newValue.toString();

        if(preference instanceof ListPreference)
        {
                ListPreference listPreference = (ListPreference) preference;
                int prefIndex = listPreference.findIndexOfValue(stringValue);
                if(prefIndex>0)
                {
                    preference.setSummary(listPreference.getEntries()[prefIndex]);
                }
        }

        else
        {
            preference.setSummary(stringValue);
        }

        return true;
    }

}
