package com.piserve.geejo.sunshine.data;

import android.annotation.TargetApi;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.location.Location;
import android.net.Uri;
import android.util.Log;

/**
 * Created by Droid Space on 2/23/2015.
 */
public class WeatherProvider extends ContentProvider
{
    //instance variable for DatabaseHelper
    private WeatherDbHelper mOpenHelper;

    //add class variable to call buildUriMatcher
    public static final UriMatcher sUriMatcher = buildUriMatcher();

    static final int WEATHER = 100;
    static final int WEATHER_WITH_LOCATION = 101;
    static final int WEATHER_WITH_LOCATION_AND_DATE = 102;
    static final int LOCATION = 300;

    /*Relationship between the tables (weather, location) is specified as constraints.
    * This relationship is implemented into our query using JOIN
    * This JOIN lets you to query weather table for values from a specific location setting*/

    //SQLiteQueryBuilder helps us to construct queries and is used as bases for query functions used in SQLite databases class.
    private static final SQLiteQueryBuilder sWeatherByLocationSettingQueryBuilder;

    static
    {
        sWeatherByLocationSettingQueryBuilder = new SQLiteQueryBuilder();

        //Fills out the "FROM" part of the SQL query.
        sWeatherByLocationSettingQueryBuilder.setTables
        (
                WeatherContract.WeatherEntry.TABLE_NAME + " INNER JOIN " + WeatherContract.LocationEntry.TABLE_NAME
                + " ON " + WeatherContract.WeatherEntry.TABLE_NAME + "." + WeatherContract.WeatherEntry.COLUMN_LOC_KEY
                + " = " + WeatherContract.LocationEntry.TABLE_NAME + "." + WeatherContract.LocationEntry._ID
        );
    }

    //Define the selection - using ? replacement syntax.
    //The selection params replaces these values.
    private static final String sLocationSettingSelection
            = WeatherContract.LocationEntry.TABLE_NAME + "." + WeatherContract.LocationEntry.COLUMN_LOCATION_SETTING + " = ? ";
    private static final String sLocationSettingWithStartDateSelection
            = WeatherContract.LocationEntry.TABLE_NAME + "." + WeatherContract.LocationEntry.COLUMN_LOCATION_SETTING + " = ? AND "
              + WeatherContract.WeatherEntry.COLUMN_DATE + " >= ? ";
    private static final String sLocationSettingWithDateSelection
            = WeatherContract.LocationEntry.TABLE_NAME + "." + WeatherContract.LocationEntry.COLUMN_LOCATION_SETTING + " =? AND "
              + WeatherContract.WeatherEntry.COLUMN_DATE + " = ? ";


    //Function to get the weather by Location setting using the same query builder.
    //We fetch the params from Uri and build a String array so that they can be substituted in our query.
    private Cursor getWeatherByLocationSetting(Uri uri, String[] projection, String sortOrder)
    {
        String locationSetting = WeatherContract.WeatherEntry.getLocationSettingFromUri(uri);
        long startDate = WeatherContract.WeatherEntry.getStartDateFromUri(uri);

        String [] selectionArgs;
        String selection;

        if(startDate==0)
        {
            selection = sLocationSettingSelection;
            selectionArgs = new String[] {locationSetting};
        }

        else
        {
            selectionArgs = new String[]{ locationSetting, Long.toString(startDate)};
            selection = sLocationSettingWithStartDateSelection;
        }

        return sWeatherByLocationSettingQueryBuilder.query
                (
                        mOpenHelper.getReadableDatabase(),
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
    }

    //Do the same for the next two queries
    private Cursor getWeatherByLocationSettingAndDate(Uri uri, String[] projection, String sortOrder)
    {
        String locationSetting = WeatherContract.WeatherEntry.getLocationSettingFromUri(uri);
        long date = WeatherContract.WeatherEntry.getDateFromUri(uri);

        return sWeatherByLocationSettingQueryBuilder.query
                (
                        mOpenHelper.getReadableDatabase(),
                        projection,
                        sLocationSettingWithDateSelection,
                        new String[]{locationSetting, Long.toString(date)},
                        null,
                        null,
                        sortOrder
                );
    }

    //URIMatcher helps to match incoming URI's to the ContentProvider integer constants.
    //This helps us understand which type of URI is passed into ContentProvider, hence perform requested operation.
    //These integer constants are then used in the switch statements.
    static UriMatcher buildUriMatcher()
    {
        /*All paths that are added to UriMatcher have a corresponding code to return when a match
        * is found The code passed into the constructor represents the code to return for the
        * root URI. And it is common to use NO_MATCH for this case. */
        //The root node doesn't need to match anything.
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        //Making a shortcut to the CONTENT_AUTHORITY for easier to read code.
        final String authority = WeatherContract.CONTENT_AUTHORITY;

        //For each type of URI you want to add, create a corresponding code
        matcher.addURI(authority, WeatherContract.PATH_WEATHER, WEATHER);
        matcher.addURI(authority, WeatherContract.PATH_WEATHER + "/*", WEATHER_WITH_LOCATION);
        matcher.addURI(authority, WeatherContract.PATH_WEATHER + "/*/#", WEATHER_WITH_LOCATION_AND_DATE);

        matcher.addURI(authority, WeatherContract.PATH_LOCATION, LOCATION);

        return matcher;
    }

    @Override
    public boolean onCreate()
    {
        //Initialize the instance variable
        mOpenHelper = new WeatherDbHelper(getContext());
        //True tells Android that the ContentProvider has been created successfully
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        Cursor retCursor;

        //Here we have a switch statement that, given a URI will determine what find of request it is, and query db accordingly.
        //Use sUriMatcher function to switch the URI. We need to fill out every type and the only place where we need to do.
        switch(sUriMatcher.match(uri))
        {
            // "weather/*/*"
            case WEATHER_WITH_LOCATION_AND_DATE:
            {
                retCursor = getWeatherByLocationSettingAndDate(uri,projection,sortOrder);
                //retCursor = null;
                break;
            }

            // "weather/*"
            case WEATHER_WITH_LOCATION:
            {
                retCursor = getWeatherByLocationSetting(uri,projection,sortOrder);
                //retCursor = null;
                break;
            }

            //Pass the parameters that comes through the ContentProvider into the query function of readable database.
            // "weather"
            case WEATHER:
            {
                //Pass all the parameters to the database.
                retCursor = mOpenHelper.getReadableDatabase().query
                           (
                                WeatherContract.WeatherEntry.TABLE_NAME, //These params are passed into the database.
                                projection,
                                selection,
                                selectionArgs,
                                null,
                                null,
                                sortOrder
                           );
                break;
            }

            // "location"
            case LOCATION:
            {
                //Pass all the parameters to the database.
                retCursor = mOpenHelper.getReadableDatabase().query
                        (
                                WeatherContract.LocationEntry.TABLE_NAME, //These params are passed into the database.
                                projection,
                                selection,
                                selectionArgs,
                                null,
                                null,
                                sortOrder
                        );
                break;
            }

            default:
            {
                throw new UnsupportedOperationException("Unknown Uri: " + uri);
            }

        }

        //Set the notification Uri for the cursor to the one that was passed into the function.
        //This causes the cursor to register a ContentObserver to watch a content URI (or descendants for changes)
        //Helps contentObserver to notify the UI when cursor changes on database insert/update
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);

        return retCursor;
    }

    //Return the MIME-Type associated with the data at the given URI.
    /*The key information that this conveys is whether we'll be returning a database cursor
    * containing a single record of type item or multiple records of type dir*/

     @Override
    public String getType(Uri uri)
    {
        //Using the UriMatcher earlier to match the given URI against the expressions we have compiled in.
        final int match = sUriMatcher.match(uri);

        //For each match return the unique MIME-Type that starts with either vnd.android.cursor.item or vnd.android.cursor.dir
        switch (match)
        {
            case WEATHER_WITH_LOCATION_AND_DATE:
                return WeatherContract.WeatherEntry.CONTENT_ITEM_TYPE;

            case WEATHER_WITH_LOCATION:
                return WeatherContract.WeatherEntry.CONTENT_TYPE;

            case WEATHER:
                return WeatherContract.WeatherEntry.CONTENT_TYPE;

            case LOCATION:
                return WeatherContract.LocationEntry.CONTENT_TYPE;

            default:
                throw new UnsupportedOperationException("Unknown URI: " +uri);
        }

    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        //Requires only base URI's for matching. Uri's used for location and date will be present in ContentValues we insert.
        final int match = sUriMatcher.match(uri);
        Uri returnUri;

        /*During insert into database we need to notify every ContentObserver that might
          have data modified by insert. (use ContentResolver)*/
        //Cursor's also notify for descendants (/Weather and /Weather/*)
        switch(match)
        {
            case WEATHER:
            {
                normalizeDate(values);

                long _id = db.insert(WeatherContract.WeatherEntry.TABLE_NAME, null, values);

                if(_id > 0)
                    returnUri = WeatherContract.WeatherEntry.buildWeatherUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }

            case LOCATION:
            {
                long _id = db.insert(WeatherContract.LocationEntry.TABLE_NAME, null, values);

                if(_id > 0)
                    returnUri = WeatherContract.LocationEntry.buildLocationUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        //Notify all the registered ContentObservers: Use uri not returnUri as it will notify Cursors of the change.
        getContext().getContentResolver().notifyChange(uri,null);
        return returnUri;
    }

    private void normalizeDate(ContentValues values)
    {
        // normalize the date value
        if (values.containsKey(WeatherContract.WeatherEntry.COLUMN_DATE))
        {
            long dateValue = values.getAsLong(WeatherContract.WeatherEntry.COLUMN_DATE);
            values.put(WeatherContract.WeatherEntry.COLUMN_DATE, WeatherContract.normalizeDate(dateValue));
        }
    }

    //Delete a row from the table according to the arguments passed. It returns the row modified,
    //rather than a URI on completion unlike Insert. Make sure to notify the ContentObservers.
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsDeleted;

        //This makes delete all rows return the number of rows deleted.
        if(selection==null)
            selection = "1";

        switch(match)
        {
            case WEATHER:
                rowsDeleted = db.delete(WeatherContract.WeatherEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case LOCATION:
                rowsDeleted = db.delete(WeatherContract.LocationEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown Uri: "+uri);

        }

        //Notify ContentResolver of the change if we've deleted some row
        //Null deletes all rows
        if(rowsDeleted!=0)
            getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    //Update the corresponding table based on the values passed. It's the same as delete.
    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsUpdated;

        switch(match)
        {
            case WEATHER:
                normalizeDate(values);
                rowsUpdated = db.update(WeatherContract.WeatherEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case LOCATION:
                rowsUpdated = db.update(WeatherContract.LocationEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown Uri: "+uri);
        }

        if(rowsUpdated!=0)
            getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }

    public int bulkInsert(Uri uri, ContentValues[] values)
    {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        switch (match)
        {
            case WEATHER:
                db.beginTransaction();
                int returnCount = 0;
                try
                {
                    for (ContentValues value : values)
                    {
                        normalizeDate(value);
                        long _id = db.insert(WeatherContract.WeatherEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                }

                finally
                {
                    db.endTransaction();
                }

                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;

            default:
                return super.bulkInsert(uri, values);
        }
    }

    // You do not need to call this method. This is a method specifically to assist the testing
    // framework in running smoothly. You can read more at:
    // http://developer.android.com/reference/android/content/ContentProvider.html#shutdown()
    @Override
    @TargetApi(11)
    public void shutdown()
    {
        mOpenHelper.close();
        super.shutdown();
    }

}