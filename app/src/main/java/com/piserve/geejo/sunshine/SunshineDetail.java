package com.piserve.geejo.sunshine;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import android.support.v7.widget.ShareActionProvider;


public class SunshineDetail extends ActionBarActivity
{
    private static final String FORECAST_SHARE_HASHTAG = "#SunshineApp";

    private String mForecastStr;

    private final String LOG_TAG = SunshineFragment.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sunshine_detail);
        if (savedInstanceState == null)
        {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sunshine_detail, menu);


        //Retrieve the share menu item
        MenuItem menuItem = menu.findItem(R.id.action_share);

        //Get the provider and hold onto it to set/change the share Intent
        ShareActionProvider mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(menuItem);

        /*Attach an intent to this ShareActionProvider. You will be able to update at any time,
          like when the user selects a new piece of data that they like to share. */
        if(mShareActionProvider!=null)
        {
            mShareActionProvider.setShareIntent(createShareForecastIntent());
        }
        else
        {
            Log.d(LOG_TAG, "Share Action Provider is null");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            startActivity(new Intent(this,SunshineSettings.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment
    {

        public PlaceholderFragment()
        {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            View rootView = inflater.inflate(R.layout.fragment_sunshine_detail, container, false);

            //The detail activity called via Intent. Inspect the intent for forecast data
            Intent intent = getActivity().getIntent();

            //Check if the intent has that value passed.
            if(intent!=null && intent.hasExtra(Intent.EXTRA_TEXT))
            {
                //Gets the forecast data string passed as Extra's from SunshineFragment
                String mForecastStr = intent.getStringExtra(Intent.EXTRA_TEXT);

                //Displays the forecast data in a textView
                ((TextView) rootView.findViewById(R.id.detailText)).setText(mForecastStr);
            }

            return rootView;
        }
    }

    /*
    Creating Share Intent
     */
    private Intent createShareForecastIntent()
    {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);

        /*Stops Activity that we are sharing to, from being placed onto the Activity stack
        * If we click on icon to go back tothe application later, you may end up in the application that
         * handles the share Intent*/
        shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        shareIntent.setType("text/plain");

        shareIntent.putExtra(Intent.EXTRA_TEXT, mForecastStr + FORECAST_SHARE_HASHTAG);
        return shareIntent;
    }
}
