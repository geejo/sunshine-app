/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.piserve.geejo.sunshine.data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.format.Time;

/**
 * Defines table and column names for the weather database.
 */
public class WeatherContract
{

    ////////////////////////////////////////////////////////////////////
    //CONTRACT = Agreement between Data Model (Storage) and Views (Presentation) describing how info is accessed
    //Agreement is implemented in a Contract class (contains list of constants) .

    /*GOING TO BIND OUR VIEW TO THE DATA MODEL -> CONTENT PROVIDER .
    * Content Provider lets us to think view data as Uri's (convenient structure for our app)
    * eg: CONTENT://COM.PISERVE.GEEJO.SUNSHINE.APP/WEATHER/682017
    * We can set views for different data, based on which URI is currently active.
    * This URI acts as a data member for intents
    * With ContentProviders and Intents we can de-couple the data being displayed from the view.
    * Also with URI and Data Location, it's easier for Database_Notify components registered to a location
    * This allows cursor to update the list and show the data
    * Content provider returns different kinds of data, typically list of items or single item
    * eg: CONTENT://COM.PISERVE.GEEJO.SUNSHINE.APP/WEATHER/682017/20140305 (Query for specific date than range
    * CONTENT PROVIDERS IS GREAT AND WE DON'T NEED TO UNDERSTAND THE DEPTHS OF THE DATA STORAGE*/

    /*The Content Authority is a name for the entire content provider (domain-name & website relation)
    * Easy way is to use package name.*/
    public static final String CONTENT_AUTHORITY = "com.piserve.geejo.sunshine";

    //Use Content Authority to create the BASE of all URI's. App's will use this to contact the Content Provider.
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    /*All possible path's (Appended to BASE_CONTENT_URI for possible URI's)
      Each Uri points to a different table */
    public static final String PATH_WEATHER = "weather";
    public static final String PATH_LOCATION = "location";

    /*Next for each table create Uri's for LocationEntry and WeatherEntry*/

    //////////////////////////////////////////////////////////////////////////////

    /* Inner class that defines the table contents of the location table */
    // Columns represented by the BaseColumns are useful, hence Contract class implements it.

    /* The inner classes within the Contract class will be used to define tables. Each table will
    implement BaseColumns and _ID column for ContentProvider integration.

    Data model will use 2 tables. One will keep information on the "Location", other will contain
        "Forecast Data" key-ed to the "Location".
    These will be tied back to the view through the Contract and ContentProvider
     */



    /*
        Inner class that defines the table contents of the location table
        Students: This is where you will add the strings.  (Similar to what has been
        done for WeatherEntry)
     */
    public static final class LocationEntry implements BaseColumns
    {

        //For SQLite: Represents Base Location for Location table.
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_LOCATION).build();

        //Adding MIME-type prefixes to know if a directory(list of items) or an item is returned by URI
        /*MIME-Type returned by Content Provider is used by an Intent to determine which Activity is
          to be launched to handle the data retrieved by given URI */

        //Cursors returned from a ContentProvider has a unique TYPE based on their content and the Base_Path returned from the query.

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_LOCATION;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_LOCATION;

        public static final String TABLE_NAME = "location";

        // The location setting string is what will be sent to OPEN WEATHER MAP
        // as the location query.
        public static final String COLUMN_LOCATION_SETTING = "location_setting";

        // Human readable location string, provided by the API.  Because for styling,
        // "Mountain View" is more recognizable than 94043.
        public static final String COLUMN_CITY_NAME = "city_name";

        // In order to uniquely pinpoint the location on the map when we launch the
        // map intent, we store the latitude and longitude as returned by openweathermap.
        public static final String COLUMN_COORD_LAT = "coord_lat";
        public static final String COLUMN_COORD_LONG = "coord_long";

        /*Building a Uri for querying the location by ID within the LocationEntry part
        * of the WeatherContract. Append the _id of the record*/

         public static Uri buildLocationUri(long id)
        {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

    }

    /* Inner class that defines the table contents of the weather table */
    public static final class WeatherEntry implements BaseColumns
    {

        //For SQLite: Represents Base Location for Weather table.
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_WEATHER).build();

        //Adding MIME-type prefixes to know if a directory(list of items) or an item is returned by URI
        /*MIME-Type returned by Content Provider is used by an Intent to determine which Activity is
          to be launched to handle the data retrieved by given URI */
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_WEATHER;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_WEATHER;

        public static final String TABLE_NAME = "weather";

        // Column with the foreign key into the location table.
        public static final String COLUMN_LOC_KEY = "location_id";
        // Date, stored as long in milliseconds since the epoch
        public static final String COLUMN_DATE = "date";
        // Weather id as returned by API, to identify the icon to be used
        public static final String COLUMN_WEATHER_ID = "weather_id";

        // Short description and long description of the weather, as provided by API.
        // e.g "clear" vs "sky is clear".
        public static final String COLUMN_SHORT_DESC = "short_desc";

        // Min and max temperatures for the day (stored as floats)
        public static final String COLUMN_MIN_TEMP = "min";
        public static final String COLUMN_MAX_TEMP = "max";

        // Humidity is stored as a float representing percentage
        public static final String COLUMN_HUMIDITY = "humidity";

        // Humidity is stored as a float representing percentage
        public static final String COLUMN_PRESSURE = "pressure";

        // Windspeed is stored as a float representing windspeed  mph
        public static final String COLUMN_WIND_SPEED = "wind";

        // Degrees are meteorological degrees (e.g, 0 is north, 180 is south).  Stored as floats.
        public static final String COLUMN_DEGREES = "degrees";


        ////////////////////////////////////////////////////////////////////////////////////////////

        /*Adding URI builder and decoder functions. This makes things convenient, as it makes
        * few places in the code aware of the actual URI encoding*/
        //This knowledge is kept in the Contract.

        /*If we are just going to have a URI with a standard INTEGER PRIMARY KEY (is the typical way
        * of distinguishing query for an Item or List of Items) we can make use of ContentUris with
        * AppendedId function*/
        public static Uri buildWeatherUri(long id)
        {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        //String such as Location_Setting can be appended with AppendPath function
        public static Uri buildWeatherLocation(String locationSetting)
        {
            return CONTENT_URI.buildUpon().appendPath(locationSetting).build();
        }

        //Making use of these functions for potentially useful query params
        /*Query params are useful for a fixed database query we have limited parameterization for.
         * (JOIN between the 2 tables) */
        public static Uri buildWeatherLocationWithStartDate(String locationSetting, long startDate)
        {
            long normalizedDate = normalizeDate(startDate);
            return CONTENT_URI.buildUpon().appendPath(locationSetting).
                    appendQueryParameter(COLUMN_DATE, Long.toString(normalizedDate)).build();
        }

        //Function for a 2-part URI that contains both location and date
        public static Uri buildWeatherLocationWithDate(String locationSetting, long date)
        {
            return CONTENT_URI.buildUpon().appendPath(locationSetting).
                    appendPath(Long.toString(normalizeDate(date))).build();
        }

        /*HELPER FUNCTIONS*/

        /*Decoding URI structure. Hides Uri structure from our code as well as for retrieving values
        from the URI . Places all knowledge in one place within Contract.*/

        public static final String getLocationSettingFromUri(Uri uri)
        {
            return uri.getPathSegments().get(1);
        }

        public static long getDateFromUri(Uri uri)
        {
            return Long.parseLong(uri.getPathSegments().get(2));
        }

        public static long getStartDateFromUri(Uri uri)
        {
            String dateString = uri.getQueryParameter(COLUMN_DATE);
            if(null!=dateString && dateString.length() > 0)

                return Long.parseLong(dateString);
            else
                return 0;
        }

    }

    // To make it easy to query for the exact date, we normalize all dates that go into
    // the database to the start of the the Julian day at UTC.
    public static long normalizeDate(long startDate)
    {
        // normalize the start date to the beginning of the (UTC) day
        Time time = new Time();
        time.setToNow();
        int julianDay = Time.getJulianDay(startDate, time.gmtoff);
        return time.setJulianDay(julianDay);
    }
}
